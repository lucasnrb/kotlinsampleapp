package com.kotlin4android

/**
 * Kotlin for Android development
 */

class Collections {

    // Mutable vs Immutable collections

    // Lists

    // Creates an immutable list of String
    val names: List<String> = listOf("Lucas", "Matias", "Dani")

    fun printImmutableListOfNames() {
        for (name in names) {
            println(name) // Will print Lucas Matias Dani
        }
    }

    fun printMutableListOfNames() {
        val mutableNames1 = names.toMutableList()
        mutableNames1.add("Andres") // Now mutable and added "Andres" to list
        println(mutableNames1) // Will print Lucas Matias Dani Andres
    }

    fun mutableExamples() {

        // A mutable list of String
        val mutableListNames: MutableList<String> = mutableListOf<String>("Juan", "Pedro")
        mutableListNames.add("Maria")
        mutableListNames.removeAt(1) // will remove Pedro
        mutableListNames[0] = "Mario" // replaces the element in index 0 with "Mario"

        print(mutableListNames) // Will print Mario Maria

        // A mutable list of mixed types
        val mutableListMixed = mutableListOf("Audi", "Toyota", 1, 2.3, 'a')
    }

    // Immutable empty list of Strings
    val emptyList: List<String> = emptyList<String>()


    // Other functions listOfNotNull(), arrayListOf()

    // Sets

    // Creates an immutable set of mixed types
    val mixedTypesSet = setOf(2, 4.454, "how", "far", 'c')
    var intSet: Set<Int> = setOf(1, 3, 4)  // Only integers types allowed

    fun mutableSet() {
        // Creates a mutable set of int types only
        val intsMutableSet: MutableSet<Int> = mutableSetOf(3, 5, 6, 2, 0)
        intsMutableSet.add(8)
        intsMutableSet.remove(3)
    }

    fun mutableHashSet() {
        // Creates a mutable hash set of int types only
        val intsHashSet: java.util.HashSet<Int> = hashSetOf(1, 2, 6, 3)
        intsHashSet.add(5)
        intsHashSet.remove(1)
    }


    // Other functions sortedSetOf(), linkedSetOf()

    // Maps

    fun immutableMap() {
        val callingCodesMap: Map<Int, String> = mapOf(54 to "Argentina", 1 to "USA")
        for ((key, value) in callingCodesMap) {
            println("$key is the calling code for $value")
        }
        // Will print
        // 54 is the calling code for Argentina
        // 1 is the calling code for USA
    }

    fun mutableMap() {
        val currenciesMutableMap: MutableMap<String, String> = mutableMapOf("Pesos" to "Argentina", "Dollars" to "USA", "Pounds" to "UK")
        println("Countries are ${currenciesMutableMap.values}") // Countries are [Argentina, USA, UK]
        println("Currencies are ${currenciesMutableMap.keys}") // Currencies are [Pesos, Dollars, Pounds]
        currenciesMutableMap.put("Real", "Brazil")
        currenciesMutableMap.remove("Dollars")


        print(currenciesMutableMap.get("Argentina")) // Will print Pesos
        print(currenciesMutableMap["Argentina"]) // Will print Pesos
    }

    fun mutableHashMap() {
        val personsHashMap: java.util.HashMap<Int, String> = hashMapOf(1 to "Ana", 2 to "Maria", 3 to "Eugenia")
        personsHashMap.put(4, "Julia")
        personsHashMap.remove(2)
        print(personsHashMap[1]) // Will print Ana
    }


    // Other functions sortedMapOf(), linkedHashMap()


    // Collection operation functions

    fun collectionFunctions() {
        val stringList: List<String> = listOf("in", "the", "club")
        print(stringList.first()) // Will print "in"
        print(stringList.last()) // Will print "club"

        val intSet: Set<Int> = setOf(3, 5, 6, 6, 6, 3)
        print(intSet.first()) // Will print 3
        print(intSet.last()) // Will print 6

        // Chaining functions
        stringList.subList(0, 1)
                .last()
    }

    // Other functions max(), average(), plus(), minus()
}