package com.kotlin4android.data.bitbucket.networking

import com.kotlin4android.data.bitbucket.model.ReposByUsernameResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * API Client to perform requests to BitBucket API.
 */
interface BitBucketApiClient {

    @GET("repositories/{username}")
    fun searchReposByUsername(@Path("username") username: String): Call<ReposByUsernameResponse>
}