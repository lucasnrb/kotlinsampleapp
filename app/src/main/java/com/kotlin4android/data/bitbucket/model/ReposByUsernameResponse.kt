package com.kotlin4android.data.bitbucket.model

/**
 * Model class of the response from BitBucket API representing repositories by username.
 */
data class ReposByUsernameResponse(val values: List<Repo>)