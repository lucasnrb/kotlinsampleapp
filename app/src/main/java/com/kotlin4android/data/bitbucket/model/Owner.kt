package com.kotlin4android.data.bitbucket.model

import com.google.gson.annotations.SerializedName

/**
 * Owner model class.
 */
data class Owner(val username: String,
                 @SerializedName("display_name")
                 val displayName: String)