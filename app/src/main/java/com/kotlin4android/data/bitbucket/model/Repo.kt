package com.kotlin4android.data.bitbucket.model

/**
 * Repository model class.
 */
// NOTE: description property can be null
data class Repo(val name: String,
                val description: String?,
                val owner: Owner)