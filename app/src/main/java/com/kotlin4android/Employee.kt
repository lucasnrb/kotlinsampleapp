package com.kotlin4android

/**
 * Kotlin for Android development
 */
abstract class Employee(private val firstName: String, private val lastName: String) {

    abstract fun earnings(): Double

    fun fullName(): String {
        return lastName + " " + firstName;
    }
}