package com.kotlin4android.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.kotlin4android.R
import com.kotlin4android.domain.model.Repo

/**
 * Main activity.
 */
class MainActivity : AppCompatActivity(), ListReposByUsernameFragment.OnRepoSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            goToSearchRepos()
        }
    }

    private fun goToSearchRepos() {
        supportFragmentManager.beginTransaction()
                .replace(R.id.container, SearchReposFragment.newInstance())
                .commit()
    }

    override fun onRepoSelected(repo: Repo) {
        val browserIntent = Intent(Intent.ACTION_VIEW)
        browserIntent.data = Uri.parse(repo.url)
        startActivity(browserIntent)
    }
}
