package com.kotlin4android.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.kotlin4android.R
import com.kotlin4android.ext.showToast
import kotlinx.android.synthetic.main.fragment_search_repos.*

/**
 * Gets username input and redirects to {@see SearchReposFragment}.
 */
class SearchReposFragment : Fragment() {

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment.
         *
         * @return A new instance of fragment SearchReposFragment.
         */
        @JvmStatic
        fun newInstance() = SearchReposFragment()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_search_repos, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Access button directly thanks to Kotlin extensions :)
        btnSearch.setOnClickListener{ goToListRepos() }
    }

    private fun goToListRepos() {
        val username = txtUsername.text.toString()
        if (username.isEmpty()) {
            activity.showToast(R.string.alertNoUsername)
        } else {
            fragmentManager.beginTransaction()
                    .replace(R.id.container, ListReposByUsernameFragment.newInstance(username))
                    .addToBackStack(ListReposByUsernameFragment.TAG)
                    .commit()
        }
    }
}
