package com.kotlin4android.ui

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kotlin4android.R
import com.kotlin4android.domain.SearchReposByUsernameInteractor
import com.kotlin4android.domain.model.Repo
import com.kotlin4android.ext.hide
import com.kotlin4android.ext.show
import com.kotlin4android.ext.showToast
import kotlinx.android.synthetic.main.fragment_list_repos_by_username.*

/**
 * Searches repositories by a given user name and shows them if any.
 */
class ListReposByUsernameFragment : Fragment(), SearchReposByUsernameInteractor.ResponseListener {

     companion object {
         const val TAG = "ListReposByUsernameFragment"

         private const val ARG_USERNAME = "username"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param username to search repos.
         *
         * @return A new instance of fragment ListReposByUsernameFragment.
         */
        @JvmStatic
        fun newInstance(username: String): ListReposByUsernameFragment {
            val fragment = ListReposByUsernameFragment()
            val args = Bundle()
            args.putString(ARG_USERNAME, username)
            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var username: String
    private lateinit var reposAdapter: ReposAdapter
    private var listener: OnRepoSelectedListener? = null

    interface OnRepoSelectedListener {
        fun onRepoSelected(repo: Repo)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnRepoSelectedListener) {
            listener = context
        } else {
            throw ClassCastException("${context.toString()} must implement OnRepoSelectedListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        username = arguments?.getString(ARG_USERNAME) ?: ""
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_list_repos_by_username, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        txtRepos.text = getString(R.string.txtRepositories, username)
        listRepos.layoutManager = LinearLayoutManager(activity)

        reposAdapter = ReposAdapter(emptyList<Repo>().toMutableList(), listener)

        listRepos.adapter = reposAdapter

        searchReposBy(username)
    }

    private fun searchReposBy(username: String) {
        progressBar.show()
        SearchReposByUsernameInteractor(username, this).execute()
    }

    override fun onResponse(repos: List<Repo>) {
        progressBar.hide()
        if (repos.isEmpty()) {
            activity.showToast(getString(R.string.alertNoReposFound, username))
        } else {
            reposAdapter.addRepos(repos)
        }
    }

    override fun onError(t: Throwable?) {
        progressBar.hide()
        activity.showToast(getString(R.string.alertErrorLoadingRepos, username))
    }
}
