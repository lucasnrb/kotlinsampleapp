package com.kotlin4android.ui

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.kotlin4android.R
import com.kotlin4android.domain.model.Repo

/**
 * Adapter for repositories data.
 */
class ReposAdapter(private var repos: List<Repo>, private val listener: ListReposByUsernameFragment.OnRepoSelectedListener?)
    : RecyclerView.Adapter<ReposAdapter.ReposViewHolder>() {

    // Single-line function
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            = ReposViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.repo_item, parent, false))

    // Single-line function
    override fun onBindViewHolder(holder: ReposViewHolder, position: Int) = holder.bind(repos[position])

    // Single-line function
    override fun getItemCount() = repos.size

    fun addRepos(repos: List<Repo>) {
        this.repos = repos
        notifyDataSetChanged()
    }

    inner class ReposViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val txtRepoName: TextView = itemView.findViewById(R.id.txtRepoName)
        private val txtRepoDescription: TextView = itemView.findViewById(R.id.txtRepoDescription)
        private lateinit var repo: Repo

        init {
            itemView.setOnClickListener { listener?.onRepoSelected(repo) }
        }

        fun bind(repo: Repo) {
            this.repo = repo
            txtRepoName.text = repo.name
            txtRepoDescription.text = repo.description
        }
    }
}
