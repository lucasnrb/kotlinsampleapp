package com.kotlin4android.domain

import com.kotlin4android.data.bitbucket.model.Repo
import com.kotlin4android.data.bitbucket.model.ReposByUsernameResponse
import com.kotlin4android.data.bitbucket.networking.BitBucketApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Interactor to search repos by a given username.
 */
class SearchReposByUsernameInteractor(private val username: String, private val listener: ResponseListener) : Callback<ReposByUsernameResponse> {

    companion object {
        private const val baseBitBucketApiUrl = "https://api.bitbucket.org/2.0/"
        private const val baseBitBucketUrl = "https://bitbucket.org"
    }

    interface ResponseListener {
        fun onResponse(repos: List<com.kotlin4android.domain.model.Repo>)

        fun onError(t: Throwable?)
    }

    private val bitBucketApiClient: BitBucketApiClient

    init {
        bitBucketApiClient = Retrofit.Builder()
                .baseUrl(baseBitBucketApiUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(BitBucketApiClient::class.java)
    }

    fun execute() {
        bitBucketApiClient.searchReposByUsername(username).enqueue(this)
    }

    override fun onResponse(call: Call<ReposByUsernameResponse>?, response: Response<ReposByUsernameResponse>?) {
        try {
            // Example using Non-Null Asserted Call to catch the exception in case of being a null reference
            if (response!!.isSuccessful && response.body() != null) {
                val reposDataModel = response.body()?.values ?: emptyList()
                val reposDomainModel = transformRepoDataModelToRepoDomainModel(reposDataModel)
                listener.onResponse(reposDomainModel)
            }
        } catch (e: Exception) {
            listener.onError(e)
        }
    }

    override fun onFailure(call: Call<ReposByUsernameResponse>?, t: Throwable?) {
        listener.onError(t)
    }

    private fun transformRepoDataModelToRepoDomainModel(repos: List<Repo>): List<com.kotlin4android.domain.model.Repo> {
        val reposDomainModel = emptyList<com.kotlin4android.domain.model.Repo>().toMutableList()

        repos.forEach {
            val repoUrl = "$baseBitBucketUrl/$username/${it.name}"
            // Named parameters
            val repoDomain = com.kotlin4android.domain.model.Repo(name = it.name, description = it.description, owner = it.owner, url = repoUrl)
            reposDomainModel.add(repoDomain)
        }

        return reposDomainModel
    }
}

