package com.kotlin4android.domain.model

import com.kotlin4android.data.bitbucket.model.Owner

/**
 * Repository model class.
 */
// NOTE: description property can be null
data class Repo(val name: String,
                val description: String?,
                // Default parameter
                val url: String = "https://bitbucket.org/", // Format https://bitbucket.org/{username}/{repo-name}
                val owner: Owner)