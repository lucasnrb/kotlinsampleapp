package com.kotlin4android

/**
 * Kotlin for Android development
 */

// Top-level function
fun sayMyName(name: String): String {
    return "Hello $name"
}

// Function basics
class Functions {

    // Member functions


    fun printHello() {
        val message = sayMyName("Lucas")
        println(message)
    }

    fun sum(a: Int, b: Int) : Int {
        return a + b
    }
}


