package com.kotlin4android.utils

import com.kotlin4android.sayMyName


/**
 * Kotlin for Android development
 */
class FunctionsUtils {

    fun printName() {
        println(sayMyName("Matias")) // output: Hello Matias
    }
}